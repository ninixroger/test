import geom0 as geo
import rectangle as rec

def test_creation ():
    p = geo.point (22 ,7)
    assert 22 == geo.getx(p)
    assert 7 == geo.gety(p)

def test_rectancle_creat(): 
    rect= rec.rectangle(200,100,400,300)
    assert rec.getx(rect) == 200
    assert rec.gety(rect) == 100
    assert rec.getw(rect) == 400
    assert rec.geth(rect) == 300

def test_center():
    rect= rec.rectangle(200,100,400,300)
    assert rec.getcentre(rect) == (400.0,250.0)